﻿using UnityEngine;

public class FollowBehavior : StateMachineBehaviour {
    
    private Transform target;

    private float speed = 3f;
    private float rotSpeed = 5f;
    private float shootingDistance = 5f;
    private float followThreshold = 20f;
    
    private static readonly int FoundTarget = Animator.StringToHash("FoundTarget");
    private static readonly int IsAttacking = Animator.StringToHash("IsAttacking");

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        
        target = animator.GetComponent<AIController>().Target;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        if (target == null) {
            
            animator.SetBool(FoundTarget, false);
            return;
        }
        
        // Get direction from AI to target
        var direction = target.position - animator.transform.position;
        
        // Calculate look angle
        var lookRot = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        
        // Apply look rotation
        var newRotation = Quaternion.Euler(0f, 0f, lookRot);
        animator.transform.rotation = Quaternion.Slerp(animator.transform.rotation, newRotation, rotSpeed * Time.deltaTime);
        
        // Make enemy move to target
        if (Vector2.Distance(animator.transform.position, target.transform.position) > shootingDistance) {

            animator.transform.position = Vector2.MoveTowards(animator.transform.position,
                target.position, speed * Time.deltaTime);
        }
        else {
            animator.SetBool(IsAttacking, true);
        }

        if (Vector2.Distance(animator.transform.position, target.transform.position) > followThreshold) {
            
            animator.SetBool(FoundTarget, false);
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        
        animator.GetComponent<AIController>().Target = null;
        target = null;
    }
}
