﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]

public class UnitCombat : MonoBehaviour {

    public Health health;

    public Gun gun;
    public Gun primaryWeapon;
    public Gun secondaryWeapon;

    public void Init(float mHealthValue) {

        this.health = GetComponent<Health>();

        health.Init(mHealthValue);
    }

    public void Shoot() {

        if (gun != null) {
            gun.Fire();
        }
    }

    public virtual void TakeDamage(float dmg) {
        this.health.ApplyDamage(dmg);
    }

    public virtual void DeathSequence() {
        
        this.gun = null;
        this.primaryWeapon = null;
        this.secondaryWeapon = null;
        this.health = null;
        Destroy(this.gameObject);
    }

    public virtual void FireGun() { }
    
    public virtual void Reload() {}
}