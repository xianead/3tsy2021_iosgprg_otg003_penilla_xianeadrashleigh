﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    public float currentHealth;
    public float maxHealth;

    public void Init(float maxHealth) {

        this.maxHealth = maxHealth;
        currentHealth = maxHealth;
    }

    public void AddHealth(float value) {

        this.currentHealth += value;

        if (currentHealth >= maxHealth) {
            currentHealth = maxHealth;
        }
    }

    public void ApplyDamage(float dmg) {

        this.currentHealth -= dmg;
    }
}