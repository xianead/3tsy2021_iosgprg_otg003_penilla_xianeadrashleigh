﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rifle : Gun {
    
    private int rifleAmmo = 40;
    private int rifleMaxClip = 30;
    private float rifleBulletSpeed = 25;

    public GameObject rifleBullet;
    public Transform rifleFirePoint;
    
    // Start is called before the first frame update
    void Start() {
        Init(rifleAmmo, rifleMaxClip,rifleBulletSpeed, rifleBullet, rifleFirePoint);
    }

    public override void Fire() {

        if (this.currentClip >= 1) {
            
            GameObject bulletClone = Instantiate(bullet);
            bulletClone.transform.position = firePoint.position;
            bulletClone.GetComponent<Rigidbody2D>().AddForce(firePoint.up * bulletSpeed, ForceMode2D.Impulse);
            this.currentClip -= 1;
        } 
        else Debug.Log("no more ammo");
    }
}
