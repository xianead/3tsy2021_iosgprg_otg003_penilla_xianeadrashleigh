﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Shotgun : Gun {

    private int shotgunAmmo = 10;
    private int shotgunMaxClip = 2;
    private float shotgunBulletSpeed = 25f;

    public GameObject shotgunBullet;
    public Transform shotgunFirePoint;
    
    // Start is called before the first frame update
    void Start() {
        
        Init(shotgunAmmo, shotgunMaxClip, shotgunBulletSpeed, shotgunBullet, shotgunFirePoint);
    }

    public override void Fire() {

        if (this.currentClip >= 1) {
            
            var defaultRotation = firePoint.rotation;

            for (int i = 0; i < 8; i++) {
                
                // Instantiate bullet at firepoint 
                GameObject bulletClone = Instantiate(bullet);
                bulletClone.transform.position = firePoint.position;
                // Create spread offset value
                var spread = Random.Range(-0.1f, 0.1f);
                // Apply offset for spread
                firePoint.rotation = new Quaternion(firePoint.rotation.x, firePoint.rotation.y,  firePoint.rotation.z + spread, firePoint.rotation.w);
                // Create bullet speed offset
                var bulletSpeedOffset = Random.Range(-5f, 0);
                // Apply spread offset value
                bulletClone.GetComponent<Rigidbody2D>().AddForce(firePoint.up * (bulletSpeed + bulletSpeedOffset), ForceMode2D.Impulse);
                // Reset rotation for the next bullet
                firePoint.rotation = defaultRotation;
            }
            this.currentClip -= 1;
        }
    }
}