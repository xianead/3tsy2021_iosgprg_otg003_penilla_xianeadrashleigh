﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class WeaponPickup : MonoBehaviour {
    
    private UILoadout loadout;

    private void Start() {

        loadout = GameMgr.Instance.UILoadout;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        
        if (other.gameObject.CompareTag("Player")) {
            
            if (this.gameObject.CompareTag("Pistol")) {
                
                AssignSecondaryWeapon(other);
                loadout.MakeSecondaryCurrent();

                // Disable world sprite and set parent
                this.GetComponent<SpriteRenderer>().enabled = false;
                SetParent(other);
                
                // Update UI Loadout
                loadout.SetToPistol();
                
                loadout.SetPistolClip(this.GetComponent<Gun>().currentClip);
                loadout.SetPistolAmmo(this.GetComponent<Gun>().currentAmmo);
                DisablePickup();
            }
            else if (this.gameObject.CompareTag("Rifle")) {
                
                // Check if player has no primary weapon, if none, equip
                if (!other.gameObject.GetComponent<UnitCombat>().primaryWeapon) {
                    
                    AssignPrimaryWeapon(other);
                    loadout.MakePrimaryCurrent();
                
                    // Update UI Loadout
                    if (!loadout.GetComponent<UILoadout>().CheckIfWeaponIsDisplayed()) {
                        loadout.SetToRifle();
                    }
                
                    loadout.SetRifleClip(this.GetComponent<Gun>().currentClip);
                    loadout.SetRifleAmmo(this.GetComponent<Gun>().currentAmmo);
                    DisablePickup();
                }
            }
            else if (this.gameObject.CompareTag("Shotgun")) {
                
                // Check if player has no primary weapon, if none, equip
                if (!other.gameObject.GetComponent<UnitCombat>().primaryWeapon) {
                    
                    AssignPrimaryWeapon(other);
                    loadout.MakePrimaryCurrent();
                
                    // Update UI Loadout
                    if (!loadout.GetComponent<UILoadout>().CheckIfWeaponIsDisplayed()) {
                        loadout.SetToShotgun();
                    }
                
                    loadout.SetShotgunClip(this.GetComponent<Gun>().currentClip);
                    loadout.SetShotgunAmmo(this.GetComponent<Gun>().currentAmmo);
                    DisablePickup();
                }
            }
        }

        if (other.gameObject.CompareTag("Enemy")) {

            if (this.gameObject.CompareTag("Pistol")) {
                AssignSecondaryWeapon(other);
            }
            else if (this.gameObject.CompareTag("Rifle")) {
                AssignPrimaryWeapon(other);
            }
            else if (this.gameObject.CompareTag("Shotgun")) {
                AssignSecondaryWeapon(other);
            }
        }
    }

    void SetParent(Collider2D collision) {
        
        // Gets the firepoint of the parent object
        this.GetComponent<Gun>().SetFirepoint(collision.gameObject.transform.GetChild(0));

        // Sets the parent to the parent loadout object
        Transform gunPos;
        (gunPos = this.transform).SetParent((collision.gameObject.transform.GetChild(1))); 
        gunPos.position = gunPos.parent.position;
    }

    void AssignPrimaryWeapon(Collider2D other) {
        
        other.gameObject.GetComponent<UnitCombat>().primaryWeapon = this.GetComponent<Gun>();

        // Check if collider has no equipped weapon, if none, equip
        if (!other.gameObject.GetComponent<UnitCombat>().gun) {
            other.gameObject.GetComponent<UnitCombat>().gun = other.gameObject.GetComponent<UnitCombat>().primaryWeapon;
        }
        
        // Disable world sprite and set parent
        this.GetComponent<SpriteRenderer>().enabled = false;
        SetParent(other);
    }

    void AssignSecondaryWeapon(Collider2D other) {
        
        other.gameObject.GetComponent<UnitCombat>().secondaryWeapon = this.GetComponent<Gun>();

        // Check if collider has no equipped weapon, if none, equip
        if (!other.gameObject.GetComponent<UnitCombat>().gun) {
            other.gameObject.GetComponent<UnitCombat>().gun = other.gameObject.GetComponent<UnitCombat>().secondaryWeapon;
        }
        
        // Disable world sprite and set parent
        this.GetComponent<SpriteRenderer>().enabled = false;
        SetParent(other);
    }

    void DisablePickup() {
        GetComponent<WeaponPickup>().enabled = false;
    }
}