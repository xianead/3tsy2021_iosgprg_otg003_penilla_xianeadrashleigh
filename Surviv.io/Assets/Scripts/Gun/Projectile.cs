﻿using System;
using UnityEngine;

public class Projectile : MonoBehaviour {
    
    private const float damage = 10f;

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.collider.CompareTag("Player")) {
            
            other.collider.GetComponent<PlayerCombat>().PlayerDamage(damage);
            Destroy(this.gameObject);
        }
        else if (other.collider.CompareTag("Enemy")) {
            
            Destroy(this.gameObject);
            other.collider.GetComponent<AIController>().AIDamage(damage);
        }
        else if (other.collider.CompareTag("Obstacle") || other.collider.CompareTag("Building") || other.collider.CompareTag("Bounds")) {
            Destroy(this.gameObject);
        }
    }
}
