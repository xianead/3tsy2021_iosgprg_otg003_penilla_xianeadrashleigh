﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : Gun {

    private int pistolAmmo = 20;
    private int pistolMaxClip = 15;
    private float pistolBulletSpeed = 25f;

    public GameObject pistolBullet;
    public Transform pistolFirePoint;

    // Start is called before the first frame update
    void Start() {
        Init(pistolAmmo, pistolMaxClip, pistolBulletSpeed, pistolBullet, pistolFirePoint);
    }

    public override void Fire() {
        
        if (this.currentClip >= 1) {
            
            GameObject bulletClone = Instantiate(bullet);
            bulletClone.transform.position = firePoint.position;
            bulletClone.GetComponent<Rigidbody2D>().AddForce(firePoint.up * bulletSpeed, ForceMode2D.Impulse);
            this.currentClip -= 1;
        } 
        else Debug.Log("no more ammo");
    }
}