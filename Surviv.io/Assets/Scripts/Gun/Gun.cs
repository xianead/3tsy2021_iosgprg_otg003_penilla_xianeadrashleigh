﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {

    // Ammo
    public int currentAmmo;
    public GameObject bullet;
    public float bulletSpeed;

    // Clip
    public int maxClip;
    public int currentClip;

    public Transform firePoint;

    public void Init(int mAmmo, int mClip, float mBulletSpeed, GameObject mBullet, Transform mFirePoint) {

        this.currentAmmo = mAmmo;
        this.maxClip = mClip;
        this.currentClip = mClip;

        this.bullet = mBullet;
        this.bulletSpeed = mBulletSpeed;
    }

    public virtual void Fire() { }

    public void Reload() {

        if (this.currentAmmo >= 1) {

            if (this.currentAmmo < this.maxClip) {

                this.currentClip = this.currentAmmo;
                this.currentAmmo = 0;
            }
            else {

                var ammoToReload = this.maxClip - this.currentClip;

                this.currentClip += ammoToReload;
                this.currentAmmo -= ammoToReload;

                if (this.currentAmmo <= 0) currentAmmo = 0;
            }
        } 
    }

    public void AddAmmo(int value) {

        this.currentAmmo += value;
    }

    public void SetFirepoint(Transform parent) {

        this.firePoint = parent;
    }
}