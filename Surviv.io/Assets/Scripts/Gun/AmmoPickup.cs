﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoPickup : MonoBehaviour {

    public int ammoToAdd;

    private void OnTriggerEnter2D(Collider2D other) {

        if (other.gameObject.CompareTag("Player")) {

            if (gameObject.CompareTag("PistolBullet")) {

                if (other.GetComponent<UnitCombat>().secondaryWeapon) {

                    var curAmmo = other.GetComponent<UnitCombat>().secondaryWeapon.currentAmmo;
                    other.GetComponent<UnitCombat>().secondaryWeapon.AddAmmo(ammoToAdd);
                    GameMgr.Instance.UILoadout.SetPistolAmmo(ammoToAdd + curAmmo);
                    Destroy(gameObject);
                }
            }
            else if (gameObject.CompareTag("RifleBullet")) {
                
                if (other.GetComponent<UnitCombat>().primaryWeapon.CompareTag("Rifle")) {

                    var curAmmo = other.GetComponent<UnitCombat>().primaryWeapon.currentAmmo;
                    other.GetComponent<UnitCombat>().primaryWeapon.AddAmmo(ammoToAdd);
                    GameMgr.Instance.UILoadout.SetRifleAmmo(ammoToAdd + curAmmo);
                    Destroy(gameObject);
                }
            }
            else if (gameObject.CompareTag("ShotgunBullet")) {
                
                if (other.GetComponent<UnitCombat>().primaryWeapon.CompareTag("Shotgun")) {
                    
                    var curAmmo = other.GetComponent<UnitCombat>().primaryWeapon.currentAmmo;
                    other.GetComponent<UnitCombat>().primaryWeapon.AddAmmo(ammoToAdd);
                    GameMgr.Instance.UILoadout.SetShotgunAmmo(ammoToAdd + curAmmo);
                    Destroy(gameObject);
                }
            }
        }
    }
}
