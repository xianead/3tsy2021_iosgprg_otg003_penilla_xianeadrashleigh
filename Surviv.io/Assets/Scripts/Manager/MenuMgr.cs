﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MenuMgr :Singleton<MenuMgr> {
    
    public InGameCanvasScript InGameCanvasScript { get; set; }
    public MenuCanvas MenuCanvas { get; set; }

    public void SetMainMenuActive() {

        MenuCanvas.gameObject.SetActive(true);
        InGameCanvasScript.gameObject.SetActive(false);
    }

    public void SetGameCanvasActive() {
        
        InGameCanvasScript.gameObject.SetActive(true);
        MenuCanvas.gameObject.SetActive(false);
    }
}
