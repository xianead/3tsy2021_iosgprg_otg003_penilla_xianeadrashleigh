﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMgr : Singleton<GameMgr> {
    
    public UILoadout UILoadout { get; set; }
    public SpawnManager SpawnManager { get; set; }
    public PlayerCombat PlayerCombat { get; set; }
    
    // Start is called before the first frame update
    void Start() {
        
        LoadScene("Menu", OnMenuLoaded);
        LoadScene("Game");
    }

    public void LoadScene(string name, Action OnCallBack = null) {
        StartCoroutine(AsyncLoadScene(name, OnCallBack));
    }

    public void ReloadGameScene(string name, Action OnCallBack = null) {
        StartCoroutine(AsyncUnloadScene(name, OnCallBack));
    }

    public void QuitGame() {
        Application.Quit();
    }

    IEnumerator AsyncLoadScene(string name, Action OnCallBack = null) {

        AsyncOperation asyncLoadScene = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);

        while (!asyncLoadScene.isDone) {
            yield return null;
        }

        if (OnCallBack != null) {
            OnCallBack.Invoke();
        }
    }
    
    IEnumerator AsyncUnloadScene(string name, Action OnCallBack = null) {

        AsyncOperation asyncLoadScene = SceneManager.UnloadSceneAsync(name);

        while (!asyncLoadScene.isDone) {
            yield return null;
        }

        if (OnCallBack != null) {
            OnCallBack.Invoke();
        }
    }

    public void StartGame() {
        SpawnManager.StartLevel();
        MenuMgr.Instance.SetGameCanvasActive();
    }

    private void OnMenuLoaded() {
        MenuMgr.Instance.SetMainMenuActive();
    }

    private void OnReloadGame() {
        LoadScene("Game");
    }

    public void GameOver() {
        MenuMgr.Instance.SetMainMenuActive();
        ReloadGameScene("Game", OnReloadGame);
    }
}
