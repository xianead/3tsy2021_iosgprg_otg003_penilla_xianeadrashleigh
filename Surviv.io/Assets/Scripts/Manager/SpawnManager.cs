﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject[] lootSpawnPoints;
    public GameObject[] aiSpawnPoints;
    public GameObject enemy;

    public Gun[] guns;
    public AmmoPickup[] ammos;
    
    // Start is called before the first frame update
    void Start() {
        GameMgr.Instance.SpawnManager = this;
    }

    void SpawnUnit() {
        
        foreach (var sp in aiSpawnPoints) {

            GameObject newEnemy = Instantiate(enemy, sp.transform.position, enemy.transform.rotation);
            newEnemy.transform.SetParent(this.transform);
        }
    }

    public void StartLevel() {
        
        foreach (var sp in lootSpawnPoints) {

            int chooseObjToSpawn = Random.Range(0, 2);

            if (chooseObjToSpawn == 0) {
                var randomGun = Random.Range(0, 3);
                var newGun = Instantiate(guns[randomGun], sp.transform.position, guns[randomGun].transform.rotation);
                newGun.transform.SetParent(transform);
            } 
            else {
                var randomAmmo = Random.Range(0, 3);
                var newAmmo = Instantiate(ammos[randomAmmo], sp.transform.position, ammos[randomAmmo].transform.rotation);
                newAmmo.transform.SetParent(transform);
            }
        }
        
        SpawnUnit();
    }
}
