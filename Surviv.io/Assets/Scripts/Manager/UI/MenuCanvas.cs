﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCanvas : MonoBehaviour {
    
    // Start is called before the first frame update
    void Start() {
        MenuMgr.Instance.MenuCanvas = this;
    }

    public void StartGame() {
        GameMgr.Instance.StartGame();
    }
    
    public void QuitGame() {
        GameMgr.Instance.QuitGame();
    }
}
