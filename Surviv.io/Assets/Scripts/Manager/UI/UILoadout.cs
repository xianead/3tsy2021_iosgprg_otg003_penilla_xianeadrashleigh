﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILoadout : MonoBehaviour {

    #region Variables

    // Weapons
    // Weapon Border
    public Image primaryWeaponBorder;
    public Image secondaryWeaponBorder;
    // Weapon Border Sprites
    public Sprite currentWeaponBorder;
    public Sprite defaultWeaponBorder;
    // Weapon Icons
    public Image primarySlot;
    public Image secondarySlot;

    // Ammos
    // Pistol
    public Text pistolClip;
    public Text pistolAmmo;
    // Rifle
    public Text rifleClip;
    public Text rifleAmmo;
    // Shotgun
    public Text shotgunClip;
    public Text shotgunAmmo;
    
    // Weapon Icon Sprites
    public Sprite[] weaponSprites;
    public Sprite defaultSprite;
    
    public FixedJoystick moveJoystick;
    public FixedJoystick rotateJoystick;
    
    #endregion

    // Start is called before the first frame update
    void Start() {

        GameMgr.Instance.UILoadout = this;
        primarySlot.sprite = defaultSprite;
        secondarySlot.sprite = defaultSprite;
    }

    #region Set Gun
    
    public void SetToShotgun() {
        primarySlot.sprite = weaponSprites[2];
    }

    public void SetToRifle() {
        primarySlot.sprite = weaponSprites[1];
    }

    public void SetToPistol() {
        secondarySlot.sprite = weaponSprites[0];
    }
    #endregion

    #region Set Ammo

    public void SetPistolAmmo(int ammoCount) {
        pistolAmmo.text = ammoCount.ToString();
    }

    public void SetPistolClip(int ammoCount) {
        pistolClip.text = ammoCount.ToString();
    }

    public void SetRifleAmmo(int ammoCount) {
        rifleAmmo.text = ammoCount.ToString();
    }

    public void SetRifleClip(int ammoCount) {
        rifleClip.text = ammoCount.ToString();
    }

    public void SetShotgunAmmo(int ammoCount) {
        shotgunAmmo.text = ammoCount.ToString();
    }

    public void SetShotgunClip(int ammoCount) {
        shotgunClip.text = ammoCount.ToString();
    }
    
    #endregion

    public void MakePrimaryCurrent() {
        primaryWeaponBorder.sprite = currentWeaponBorder;
    }
    
    public void MakePrimaryDefault() {
        primaryWeaponBorder.sprite = defaultWeaponBorder;
    }
    
    public void MakeSecondaryCurrent() {
        secondaryWeaponBorder.sprite = currentWeaponBorder;
    }

    public void MakeSecondaryDefault() {
        secondaryWeaponBorder.sprite = defaultWeaponBorder;
    }
    
    public bool CheckIfWeaponIsDisplayed() {

        if (primarySlot.sprite == defaultSprite) return false;
        return true;
    }
}
