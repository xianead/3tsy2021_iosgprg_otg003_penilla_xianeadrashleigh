﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickSetter : MonoBehaviour {
    
    [SerializeField] private FixedJoystick moveJoystick;
    [SerializeField] private FixedJoystick rotateJoystick;
    
    // Start is called before the first frame update
    void Start() {
        GameMgr.Instance.UILoadout.moveJoystick = moveJoystick;
        GameMgr.Instance.UILoadout.rotateJoystick = rotateJoystick;
    }
}
