﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using UnityEditor;
using UnityEngine;

public class InGameCanvasScript : MonoBehaviour {
    
    private PlayerCombat playerCombat;

    [SerializeField] private HealthBar healthBar;

    private void Start() {
        MenuMgr.Instance.InGameCanvasScript = this;
    }

    // Update is called once per frame
    void Update(){
        
        if (!playerCombat) {
            playerCombat = GameMgr.Instance.PlayerCombat;
            
            if (playerCombat) {
                healthBar.SetMaxValue(playerCombat.health.maxHealth);
            }

            return;
        }
        
        healthBar.SetHealth(playerCombat.health.currentHealth);
    }

    public void PlayerFire() {
        playerCombat.OnPointerDown();
    }

    public void StopFire() {
        playerCombat.OnPointerUp();
    }

    public void SwitchPlayerWeapon() {
        playerCombat.SwitchWeapon();
    }

    public void PlayerReload() {
        playerCombat.Reload();
    }
}
