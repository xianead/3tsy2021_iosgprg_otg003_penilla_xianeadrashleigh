﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    [SerializeField] private Transform target;

    // Update is called once per frame
    void LateUpdate() {

        if (target) {
            transform.position = new Vector3(target.position.x, target.position.y, this.transform.position.z);
        }
    }
}