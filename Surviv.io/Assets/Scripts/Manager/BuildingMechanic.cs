﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingMechanic : MonoBehaviour {

    [SerializeField] private GameObject roof;
    
    private void OnTriggerStay2D(Collider2D other) {
        
        if (other.CompareTag("Player")) {
            roof.SetActive(false);
        }
    }
    
    private void OnTriggerExit2D(Collider2D other) {
        roof.SetActive(true);
    }
}