﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    private Rigidbody2D rb;
    
    private float maxPos = 49f;

    private float smoothAngleHolder, smoothAngleTime;
    [SerializeField] private float moveSpeed;

    private FixedJoystick moveJoystick;
    private FixedJoystick rotateJoystick;

    void Awake() {

        rb = this.GetComponent<Rigidbody2D>();
    }

    void Update() {

        if (!moveJoystick && !rotateJoystick) {
            
            moveJoystick = GameMgr.Instance.UILoadout.moveJoystick;
            rotateJoystick = GameMgr.Instance.UILoadout.rotateJoystick;
        }
        
        var position = transform.position;
        
        position = new Vector3(Mathf.Clamp(position.x, -maxPos, maxPos), Mathf.Clamp(position.y, -maxPos, maxPos), position.z);
        transform.position = position;
    }

    void LateUpdate() {
        
        // Movement
        rb.velocity = new Vector2(moveJoystick.Horizontal * moveSpeed, moveJoystick.Vertical * moveSpeed);
        
        //transform.Translate(new Vector2(moveJoystick.Horizontal * moveSpeed, moveJoystick.Vertical * moveSpeed * Time.deltaTime));
        
        Debug.Log("Joystick movement: " + moveJoystick.Horizontal);

        // Rotation 
        float xAxis = rotateJoystick.Horizontal;
        float yAxis = rotateJoystick.Vertical;
        float zAxis = Mathf.Atan2(xAxis, yAxis) * Mathf.Rad2Deg;

        if (xAxis != 0 && yAxis != 0) {

            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.z, zAxis, ref smoothAngleHolder, smoothAngleTime);
            transform.eulerAngles = new Vector3(0, 0, -angle);
        }
    }
}