﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : UnitCombat {

    public Transform gunPoint;
    
    public int pistolAmmoPickup, rifleAmmoPickup, shotgunAmmoPickup;

    // Start is called before the first frame update
    void Start() {

        Init(100f);

        GameMgr.Instance.PlayerCombat = this;
    }

    public void PlayerDamage(float dmg) {

        TakeDamage(dmg);

        if (this.health.currentHealth <= 0) {
            DeathSequence();
        }
    }

    public override void DeathSequence() {
        
        base.DeathSequence();
        GameMgr.Instance.GameOver();
    }

    public void SwitchWeapon() {

        if (this.gun == this.primaryWeapon) {
            // Make secondary the current weapon
            this.gun = this.secondaryWeapon;
            
            // Update UI
            GameMgr.Instance.UILoadout.MakeSecondaryCurrent();
            GameMgr.Instance.UILoadout.MakePrimaryDefault();
        }
        else {
            // Make primary the current weapon
            this.gun = primaryWeapon;
            
            // Update UI
            GameMgr.Instance.UILoadout.MakePrimaryCurrent();
            GameMgr.Instance.UILoadout.MakeSecondaryDefault();
        }
    }
    
    public override void FireGun() {
        
        if (this.gun) {
            
            gun.Fire();

            if (this.gun.CompareTag("Pistol")) {
                GameMgr.Instance.UILoadout.SetPistolClip(this.gun.currentClip);
            }
            else if (this.gun.CompareTag("Rifle")) {
                GameMgr.Instance.UILoadout.SetRifleClip(this.gun.currentClip);
            }
            else if (this.gun.CompareTag("Shotgun")) {
                GameMgr.Instance.UILoadout.SetShotgunClip(this.gun.currentClip);
            }
        }
    }

    public override void Reload() {

        if (this.gun) {
            
            gun.Reload();
            
            if (this.gun.CompareTag("Pistol")) {
                
                GameMgr.Instance.UILoadout.SetPistolAmmo(this.gun.currentAmmo);
                GameMgr.Instance.UILoadout.SetPistolClip(this.gun.currentClip);
            }
            else if (this.gun.CompareTag("Rifle")) {
                
                GameMgr.Instance.UILoadout.SetRifleAmmo(this.gun.currentAmmo);
                GameMgr.Instance.UILoadout.SetRifleClip(this.gun.currentClip);
            }
            else if (this.gun.CompareTag("Shotgun")) {
                GameMgr.Instance.UILoadout.SetShotgunAmmo(this.gun.currentAmmo);
                GameMgr.Instance.UILoadout.SetShotgunClip(this.gun.currentClip);
            }
        }
    }

    public void OnPointerDown() {

        const float automaticFireRate = .5f;
        const float semiAutoFireRate = Mathf.Infinity;

        if (!this.gun) return;
        
        if (this.gun.CompareTag("Rifle")) {
            InvokeRepeating(nameof(FireGun), 0f, automaticFireRate);
        }

        if (this.gun.CompareTag("Pistol")) {
            InvokeRepeating(nameof(FireGun), 0f, semiAutoFireRate);
        }
        
        if (this.gun.CompareTag("Shotgun")) {
            InvokeRepeating(nameof(FireGun), 0f, semiAutoFireRate);
        }
    }

    public void OnPointerUp() {

        CancelInvoke();
    }
    
}