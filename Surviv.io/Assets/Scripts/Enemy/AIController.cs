﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : UnitCombat {
    
    public Transform Target { get; set; }
    public Transform LootTarget { get; set; }
    
    private float healthPoints = 100f;
    private float maxPos = 48f;

    private void Start() {
        
        Init(healthPoints);
    }

    private void Update() {
        
        var position = transform.position;
        position = new Vector3(Mathf.Clamp(position.x, -maxPos, maxPos), Mathf.Clamp(position.y, -maxPos, maxPos), position.z);
        transform.position = position;
    }

    public override void FireGun() {

        if (!this.gun) return;

        if (this.gun.currentClip <= 0) Reload();
        gun.Fire();
    }

    public override void Reload() {
        
        if (!this.gun) return;
        
        gun.Reload();
    }

    public void AIDamage(float dmg) {
        
        TakeDamage(dmg);
        
        if (this.health.currentHealth <= 0) {
            DeathSequence();
        }
    }
}
