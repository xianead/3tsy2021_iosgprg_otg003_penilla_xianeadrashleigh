﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LootingBehavior : StateMachineBehaviour {
    
    private Transform target;
    
    private float speed = 3f;
    private float rotSpeed = 5f;
    
    private static readonly int IsLooting = Animator.StringToHash("IsLooting");

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        
        target = animator.GetComponent<AIController>().LootTarget;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        // Get direction from AI to target
        var direction = target.position - animator.transform.position;
        
        // Calculate look angle
        var lookRot = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        
        // Apply look rotation
        var newRotation = Quaternion.Euler(0f, 0f, lookRot);
        animator.transform.rotation = Quaternion.Slerp(animator.transform.rotation, newRotation, rotSpeed * Time.deltaTime);
        
        animator.transform.position = Vector2.MoveTowards(animator.transform.position, target.position, speed * Time.deltaTime);

        if (animator.GetComponent<UnitCombat>().gun) {
            animator.SetBool(IsLooting, false);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        
        animator.GetComponent<AIController>().LootTarget = null;
        target = null;
    }

}
