﻿using UnityEngine;
using Random = UnityEngine.Random;

public class PatrolBehavior : StateMachineBehaviour {
    
    private Collider2D[] enemies;
    private Collider2D[] loot;
    
    private Transform currentTransform;
    private float enemyRadius = 10f;
    private float lootRadius = 15f;
    private float checkTimer;
    private float checkRate = .5f;

    private Vector3 targetPos;
    private Vector3 randMovement;
    private float moveSpeed = 3f;
    private float rotSpeed = 5f;
    private float maxPos = 48f;

    [SerializeField] private LayerMask enemyLayerMask;
    [SerializeField] private LayerMask lootLayerMask;
    
    private static readonly int FoundTarget = Animator.StringToHash("FoundTarget");
    private static readonly int IsLooting = Animator.StringToHash("IsLooting");

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        currentTransform = animator.transform;
        randMovement = GetRandomMovement();
        targetPos = animator.transform.position + randMovement;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        
        var direction = targetPos - animator.transform.position;
        var lookRot = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        var newRotation = Quaternion.Euler(0f, 0f, lookRot);
        animator.transform.rotation = Quaternion.Slerp(animator.transform.rotation, newRotation, rotSpeed * Time.deltaTime);
        
        animator.transform.position = Vector2.MoveTowards(animator.transform.position, targetPos, moveSpeed * Time.deltaTime);

        if (Vector3.Distance(animator.transform.position, targetPos) < .3f 
            || (animator.transform.position.x > maxPos) 
            || (animator.transform.position.x < -maxPos) 
            || (animator.transform.position.y > maxPos) 
            || (animator.transform.position.y < -maxPos)) {
            
            randMovement = GetRandomMovement();
            targetPos = animator.transform.position + randMovement;
        }

        checkTimer += Time.deltaTime;
        if (checkTimer >= checkRate) {

            if (animator.GetComponent<AIController>().Target == null && animator.GetComponent<UnitCombat>().gun) {
                
                animator.GetComponent<AIController>().Target = CheckIfEnemyDetected();
                checkTimer = 0;

                if (animator.GetComponent<AIController>().Target) {
                
                    animator.SetBool(FoundTarget, true);
                }
            }
            else if (animator.GetComponent<UnitCombat>().gun == null) {

                animator.GetComponent<AIController>().LootTarget = CheckForLoot();
                checkTimer = 0;

                if (animator.GetComponent<AIController>().LootTarget) {
                    
                    animator.SetBool(IsLooting, true);
                }
            }
        }
    }
    
    Transform CheckIfEnemyDetected() {
        
        enemies = Physics2D.OverlapCircleAll(currentTransform.position, enemyRadius, enemyLayerMask);

        foreach (var col in enemies) {

            if (col.GetComponent<UnitCombat>() && currentTransform != col.transform) {
                
                return col.transform;
            }
        }
        
        return null;
    }

    Transform CheckForLoot() {

        loot = Physics2D.OverlapCircleAll(currentTransform.position, lootRadius, lootLayerMask);

        if (loot.Length <= 0) {
            return null;
        }
        
        var distanceToClosestLoot = Mathf.Infinity;
        Transform closestLoot = null;
        
        foreach (Collider2D currentLoot in loot) {
            
            var distanceToLoot = (currentLoot.transform.position - currentTransform.position).sqrMagnitude;

            if (distanceToLoot < distanceToClosestLoot && currentLoot.transform.parent == null) {

                distanceToClosestLoot = distanceToLoot;
                closestLoot = currentLoot.transform;
            }
        }
        return closestLoot;
    }

    Vector3 GetRandomMovement() {

        Vector3 newRandMove;
        newRandMove.x = Random.Range(-10f, 10f);
        newRandMove.y = Random.Range(-10f, 10f);
        newRandMove.z = 0;

        return newRandMove;
    }
}
