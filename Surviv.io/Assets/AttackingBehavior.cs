﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackingBehavior : StateMachineBehaviour {

    private Transform target;
    
    private const float rotSpeed = 5f;
    private const float fireRate = 0.75f;
    private float fireTimer;
    
    private float shootingThreshold = 15f;
    
    private static readonly int IsAttacking = Animator.StringToHash("IsAttacking");

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        target = animator.GetComponent<AIController>().Target;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        if (target == null) {
            animator.SetBool(IsAttacking, false);
            return;
        }
        
        if (Vector3.Distance(animator.transform.position, target.position) > shootingThreshold) {
            animator.SetBool(IsAttacking, false);
        }
        
        // Get direction from AI to target
        var direction = target.position - animator.transform.position;
        // Calculate look angle
        var lookRot = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        // Apply look rotation
        var newRotation = Quaternion.Euler(0f, 0f, lookRot);
        animator.transform.rotation = Quaternion.Slerp(animator.transform.rotation, newRotation, rotSpeed * Time.deltaTime);
        
        // Fire in the direction of the target 
        if (fireTimer >= fireRate) {
            animator.GetComponent<AIController>().FireGun();
            fireTimer = 0;
        }
        else {
            fireTimer += Time.deltaTime;
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        animator.GetComponent<AIController>().Target = null;
    }
}
