﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerChild : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag.Contains("Enemy"))
        {
            GetComponentInParent<Player>().AddEnemyToList(other.gameObject);
            other.GetComponent<Enemy>().NowInRange();
        }
    }
}