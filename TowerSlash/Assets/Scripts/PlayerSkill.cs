﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSkill : MonoBehaviour
{
    public List<GameObject> enemyList = new List<GameObject>();

    float timeLeft;
    int scoreCount;
    int streakCount;

    bool isSkillActive = false;
    private bool isDashing = false;

    public Text streak;
    public Text score;
    public Text skillText_1;
    public Text skillText_2;
    public Button attackSkillButton;
    public Button slowMoSkillButton;

    public TimeManager timeManager;

    [SerializeField] Camera cam;

    Player player;

    // Start is called before the first frame update
    void Start()
    {
        streakCount = 0;
        scoreCount = 0;

        timeManager = timeManager.GetComponent<TimeManager>();
        player = this.GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        printStreakCount();
        printScore();
        printSkills();
    }

    public int getScore()
    {
        return this.scoreCount;
    }

    public void addStreak()
    {
        streakCount++;
    }

    public void addScore()
    {
        scoreCount++;
    }

    private void printStreakCount()
    {
        streak.text = "Streak: " + streakCount.ToString();
    }

    private void printScore()
    {
        score.text = "Score: " + scoreCount.ToString();
    }

    private void printSkills()
    {
        if (streakCount >= 7)
        {
            // Skill 1
            skillText_1.text = "Attack Skill Up!";
            attackSkillButton.interactable = true;

            // Skill 2
            skillText_2.text = "Slow Mo Up!";
            slowMoSkillButton.interactable = true;
        }
        else
        {
            // Skill 1
            skillText_1.text = "Not yet";
            attackSkillButton.interactable = false;

            // Skill 2
            skillText_2.text = "Not yet";
            slowMoSkillButton.interactable = false;
        }
    }

    public void attackSkill()
    {
        enemyList = player.getEnemyList();

        if (enemyList.Count > 0 && streakCount >= 7)
        {
            streakCount = 0;

            for (int i = 0; i < 3; i++)
            {
                GameObject closestEnemy = player.CheckNearestTarget();

                enemyList.RemoveAt(0);
                closestEnemy.GetComponent<Enemy>().Destroy();
            }
        }
        else
        {
            Debug.Log("No enemy");
        }
    }

    private IEnumerator slowMoSkill()
    {
        timeManager.DoSlowMotion();
        cam.GetComponent<PostProcessing>().StartSlowMoVFX();

        streakCount = 0;

        yield return 1;
    }

    private IEnumerator returnVFX()
    {
        cam.GetComponent<PostProcessing>().EndSlowMoVFX();
        yield return 1;
    }

    public IEnumerator slowMoSequence()
    {
        yield return StartCoroutine(slowMoSkill());

        yield return new WaitForSecondsRealtime(2);

        yield return StartCoroutine(returnVFX());
    }

    public void startSlowSeq()
    {
        StartCoroutine(slowMoSequence());
    }
}