﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Tooltip("1 - W, 2 - A, 3 - S, 4 - D")]
    [SerializeField] private int assignedDirection;

    [SerializeField] private string color;

    [SerializeField] bool inRange = false;

    [SerializeField] float rotateTimer;

    [SerializeField] float rotateRate;

    Quaternion defaultRotation;

    private void Awake()
    {
        SetRotation();
    }

    private void Start()
    {
        rotateTimer = 0;
        defaultRotation = transform.rotation;
    }

    private void Update()
    {
        if (!inRange)
        {
            rotateTimer += 1 * Time.deltaTime;

            if (rotateTimer >= rotateRate)
            {
                RandomRotate();
                rotateTimer = 0;
            }
        }
        else
        {
            ResetRotation();
        }
    }

    public bool checkForDirection(int direction)
    {
        if (direction == assignedDirection)return true;
        else return false;
    }

    void SetRotation()
    {
        // Green Arrows
        if (this.assignedDirection == 4 && color == "Green")
        {
            transform.Rotate(new Vector3(0, 0, 270));
        }
        else if (this.assignedDirection == 3 && color == "Green")
        {
            transform.Rotate(new Vector3(0, 0, 180));
        }
        else if (this.assignedDirection == 2 && color == "Green")
        {
            transform.Rotate(new Vector3(0, 0, 90));
        }

        // Red Arrows
        if (this.assignedDirection == 4 && color == "Red")
        {
            transform.Rotate(new Vector3(0, 0, 90));
        }
        else if (this.assignedDirection == 2 && color == "Red")
        {
            transform.Rotate(new Vector3(0, 0, 270));
        }
        else if (this.assignedDirection == 1 && color == "Red")
        {
            transform.Rotate(new Vector3(0, 0, 180));
        }
    }

    void RandomRotate()
    {
        Vector3 rotation = new Vector3(transform.rotation.x, transform.rotation.y, 90f);
        transform.Rotate(rotation);
    }

    void ResetRotation()
    {
        transform.rotation = defaultRotation;
    }

    public void NowInRange()
    {
        inRange = true;
    }

    public void Destroy()
    {
        Destroy(this.gameObject);
    }

    public void setRotateTimer(float newRotateRate)
    {
        this.rotateRate = newRotateRate;
    }

}