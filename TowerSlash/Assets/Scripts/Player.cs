﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    // MOVEMENT CONTROLS
    [SerializeField] private float speed, baseSpeed, dashSpeed;
    private Rigidbody2D rb;
    private bool isDashing = false;

    [SerializeField] private float startDashTimer;
    private float currentDashTimer;

    // ENEMY RELATED VARIABLES
    int direction;
    public List<GameObject> enemyList = new List<GameObject>();

    // SWIPE CONTROLS
    private Vector2 startPos;
    private Vector2 endPos;

    public float maxSwipeTime;
    public float minSwipeDistance;

    private float swipeStartTime;
    private float swipeEndTime;

    private float swipeTime;
    private float swipeLength;

    PlayerSkill skill;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        skill = this.GetComponent<PlayerSkill>();

        speed = baseSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        DirectionSetter();
        TouchControls();
        //KeyboardControls();

        PlayerDash();
    }

    private void LateUpdate()
    {
        rb.velocity = new Vector2(speed, rb.velocity.y);
    }

    // DONE
    private void DirectionSetter()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            direction = 1;
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            direction = 2;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            direction = 3;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            direction = 4;
        }
    }

    // DONE
    private void PlayerDash()
    {
        if (isDashing)
        {
            speed = dashSpeed;

            currentDashTimer -= Time.deltaTime;
            UnityEngine.Debug.Log(currentDashTimer);

            if (currentDashTimer <= 0f)
            {
                currentDashTimer = 0;
            }

            if (currentDashTimer == 0)
            {
                speed = baseSpeed;
                isDashing = false;
            }
        }
    }

    // DONE 
    private void TouchControls()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                swipeStartTime = Time.time;
                startPos = touch.position;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                swipeEndTime = Time.time;
                endPos = touch.position;
                swipeTime = swipeEndTime - swipeStartTime;
                swipeLength = (endPos - startPos).magnitude;

                if (swipeTime < maxSwipeTime && swipeLength > minSwipeDistance)
                {
                    GameObject closestEnemy = CheckNearestTarget();

                    Vector2 distance = endPos - startPos;
                    float xDistance = Mathf.Abs(distance.x);
                    float yDistance = Mathf.Abs(distance.y);

                    if (xDistance > yDistance)
                    {
                        if (distance.x > 0)
                        {
                            direction = 4; // Right
                        }
                        else if (distance.x < 0)
                        {
                            direction = 2; // Left
                        }
                    }
                    else if (yDistance > xDistance)
                    {
                        if (distance.y > 0)
                        {
                            direction = 1; // Up
                        }
                        else if (distance.y < 0)
                        {
                            direction = 3; // Down
                        }
                    }

                    if (closestEnemy.GetComponent<Enemy>().checkForDirection(direction)) // Checks if direction is correct
                    {
                        skill.addScore();
                        skill.addStreak();
                        enemyList.RemoveAt(0);
                        closestEnemy.GetComponent<Enemy>().Destroy();
                    }
                    else
                    {
                        Destroy(this.gameObject);
                        SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
                    }
                }
                else
                {
                    isDashing = true;
                    currentDashTimer = startDashTimer;
                    rb.velocity = Vector2.zero;
                }
            }
        }
    }

    // DONE
    private void KeyboardControls()
    {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) ||
            Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D))
        {
            GameObject closestEnemy = CheckNearestTarget();

            if (closestEnemy.GetComponent<Enemy>().checkForDirection(direction) == true) // Checks if direction is correct
            {
                enemyList.RemoveAt(0);
                closestEnemy.GetComponent<Enemy>().Destroy();

                skill.addStreak();
                skill.addScore();
            }
            else
            {
                Destroy(this.gameObject);
                SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            isDashing = true;
            currentDashTimer = startDashTimer;
            rb.velocity = Vector2.zero;
        }
    }

    // DONE
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
            SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
        }
    }

    // DONE
    public GameObject CheckNearestTarget()
    {
        if (enemyList.Count <= 0)
        {
            return null;
        }

        float distanceToClosestEnemy = Mathf.Infinity;
        GameObject closestEnemy = null;

        foreach (GameObject currentEnemy in enemyList)
        {
            float distanceToEnemy = (currentEnemy.transform.position - this.transform.position).sqrMagnitude;
            if (distanceToEnemy < distanceToClosestEnemy)
            {
                distanceToClosestEnemy = distanceToEnemy;
                closestEnemy = currentEnemy;
            }
        }
        return closestEnemy;
    }

    // DONE
    public void AddEnemyToList(GameObject enemy)
    {
        if (enemyList.Exists(x => x.gameObject == enemy))
        {
            return;
        }
        else
        {
            enemyList.Add(enemy.gameObject);
        }
    }

    public List<GameObject> getEnemyList()
    {
        return this.enemyList;
    }

    public void setSpeed(float newSpeed)
    {
        this.speed = newSpeed;
    }

    public float getPlayerBaseSpeed()
    {
        return this.baseSpeed;
    }
}