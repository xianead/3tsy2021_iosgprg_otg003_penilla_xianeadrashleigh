﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] bool inRange = false;

    [SerializeField] GameObject[] enemies;

    [SerializeField] Transform playerPosition;

    [SerializeField] float spawnRate, defaultSpawnRate, bossPosOffset;

    [SerializeField] GameObject boss;

    bool isBossSpawned = false;

    GameObject player;

    Vector2 offset = new Vector2(20f, 0f);

    private void Start()
    {
        this.spawnRate = defaultSpawnRate;
        StartCoroutine(Spawner());
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        SpawnBoss();
    }

    IEnumerator Spawner()
    {
        while (true)
        {
            int rand = Random.Range(0, 7);

            Instantiate(enemies[rand], new Vector3(playerPosition.position.x + offset.x, playerPosition.position.y, playerPosition.position.z), Quaternion.identity);

            yield return new WaitForSeconds(spawnRate);
        }
    }

    void SpawnBoss()
    {
        if (isBossSpawned == false)
        {
            if (player.GetComponent<PlayerSkill>().getScore() == 25)
            {
                Instantiate(boss, new Vector3(playerPosition.position.x + offset.x, playerPosition.position.y + bossPosOffset, playerPosition.position.z), Quaternion.identity);
                isBossSpawned = true;
            }
        }
    }

    public void setSpawnRate(float newSpawnRate)
    {
        this.spawnRate = newSpawnRate;
    }

    public void setDefaultSpawnRate()
    {
        this.spawnRate = defaultSpawnRate;
    }
}