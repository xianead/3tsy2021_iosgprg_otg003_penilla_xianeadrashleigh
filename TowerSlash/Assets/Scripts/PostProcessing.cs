﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessing : MonoBehaviour
{
    public PostProcessVolume profile;

    [SerializeField] float smoothTime = 200000f;

    public void StartSlowMoVFX()
    {
        ChromaticAberration ca;

        profile.profile.TryGetSettings(out ca);

        if (ca != null)
        {
            float currentIntensity = ca.intensity.value;
            ca.intensity.value = Mathf.Lerp(-1f, 1f, smoothTime);
        }
    }

    public void EndSlowMoVFX()
    {
        ChromaticAberration ca;

        profile.profile.TryGetSettings(out ca);

        if (ca != null)
        {
            float currentIntensity = ca.intensity.value;
            ca.intensity.value = Mathf.Lerp(currentIntensity, -1f, smoothTime);
        }
    }
}