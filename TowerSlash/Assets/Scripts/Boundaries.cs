﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundaries : MonoBehaviour
{
    float xOffset = 75f;

    public void movePosition()
    {
        this.transform.position = new Vector2(this.transform.position.x + xOffset,
            this.transform.position.y);
    }
}