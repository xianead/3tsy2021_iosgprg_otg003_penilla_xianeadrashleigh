﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    private GameObject player;
    private GameObject enemySpawner;
    private Rigidbody2D rb;

    float newSpeed = 12f;
    float newSpawnRate = 0.45f;
    float playerBaseSpeed;

    float deathTimer = 7f;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        enemySpawner = GameObject.FindGameObjectWithTag("EnemySpawner");

        rb = this.GetComponent<Rigidbody2D>();

        playerBaseSpeed = player.GetComponent<Player>().getPlayerBaseSpeed();
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(startTimer());

        speedUp();
    }

    private void LateUpdate()
    {
        rb.velocity = new Vector2(newSpeed, rb.velocity.y);
    }

    void speedUp()
    {
        player.GetComponent<Player>().setSpeed(newSpeed);
        enemySpawner.GetComponent<EnemySpawner>().setSpawnRate(newSpawnRate);
    }

    IEnumerator startTimer()
    {
        Debug.Log("start");
        yield return new WaitForSecondsRealtime(deathTimer);

        Debug.Log("hasta la vista baby");
        Destroy(this.gameObject);
    }

    void slowDown()
    {
        player.GetComponent<Player>().setSpeed(playerBaseSpeed);
        enemySpawner.GetComponent<EnemySpawner>().setDefaultSpawnRate();
    }

    void OnDestroy()
    {
        slowDown();
    }
}